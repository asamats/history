<?php

use app\models\History;
use app\widgets\HistoryList\helpers\HistoryListHelper;
use kartik\export\ExportMenu;
use yii\web\View;
use yii\data\ActiveDataProvider;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$filename = 'history-' . time();

ini_set('max_execution_time', 0);
ini_set('memory_limit', '2048M');

echo ExportMenu::widget([
    'id' => 'history_to_csv',
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'ins_ts',
            'label' => Yii::t('app', 'Date'),
            'format' => 'datetime'
        ],
        [
            'label' => Yii::t('app', 'User'),
            'value' => function (History $model) {
                return isset($model->user) ? $model->user->username : Yii::t('app', 'System');
            }
        ],
        [
            'label' => Yii::t('app', 'Type'),
            'value' => function (History $model) {
                return $model->object;
            }
        ],
        [
            'label' => Yii::t('app', 'Event'),
            'value' => function (History $model) {
                return $model->eventText;
            }
        ],
        [
            'label' => Yii::t('app', 'Message'),
            'value' => function (History $model) {
                return strip_tags(HistoryListHelper::getBodyByModel($model));
            }
        ]
    ],
    'batchSize' => 2000,
    'filename' => $filename,
]);