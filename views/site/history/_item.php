<?php

use app\models\Call;
use app\models\Customer;
use app\models\History;
use app\models\Sms;

/** @var History $model */
switch ($model->event) {
    case History::EVENT_CREATED_TASK:
    case History::EVENT_COMPLETED_TASK:
    case History::EVENT_UPDATED_TASK:
        echo $this->render('_item_common', [
            'model' => $model,
            'iconClass' => 'fa-check-square bg-yellow',
            'footerDatetime' => $model->ins_ts,
        ]);
        break;
    case History::EVENT_INCOMING_SMS:
    case History::EVENT_OUTGOING_SMS:
        echo $this->render('_item_common', [
            'model' => $model,
            'footer' => $model->sms->direction == Sms::DIRECTION_INCOMING ?
                Yii::t('app', 'Incoming message from {number}', [
                    'number' => $model->sms->phone_from ?? ''
                ]) : Yii::t('app', 'Sent message to {number}', [
                    'number' => $model->sms->phone_to ?? ''
                ]),
            'iconIncome' => $model->sms->direction == Sms::DIRECTION_INCOMING,
            'footerDatetime' => $model->ins_ts,
            'iconClass' => 'icon-sms bg-dark-blue'
        ]);
        break;
    case History::EVENT_OUTGOING_FAX:
    case History::EVENT_INCOMING_FAX:
        echo $this->render('_item_common', [
            'model' => $model,
            'footer' => Yii::t('app', '{type} was sent to {group}', [
                'type' => $model->fax ? $model->fax->getTypeText() : 'Fax',
            ]),
            'footerDatetime' => $model->ins_ts,
            'iconClass' => 'fa-fax bg-green'
        ]);
        break;
    case History::EVENT_CUSTOMER_CHANGE_TYPE:
        echo $this->render('_item_statuses_change', [
            'model' => $model,
            'oldValue' => Customer::getTypeTextByType($model->getDetailOldValue('type')),
            'newValue' => Customer::getTypeTextByType($model->getDetailNewValue('type'))
        ]);
        break;
    case History::EVENT_CUSTOMER_CHANGE_QUALITY:
        echo $this->render('_item_statuses_change', [
            'model' => $model,
            'oldValue' => Customer::getQualityTextByQuality($model->getDetailOldValue('quality')),
            'newValue' => Customer::getQualityTextByQuality($model->getDetailNewValue('quality')),
        ]);
        break;

    case History::EVENT_INCOMING_CALL:
    case History::EVENT_OUTGOING_CALL:
        $answered = $model->call && $model->call->status == Call::STATUS_ANSWERED;

        echo $this->render('_item_common', [
            'model' => $model,
            'content' => $model->call->comment ?? '',
            'footerDatetime' => $model->ins_ts,
            'iconClass' => $answered ? 'md-phone bg-green' : 'md-phone-missed bg-red',
            'iconIncome' => $answered && $model->call->direction == Call::DIRECTION_INCOMING
        ]);
        break;

    default:
        echo $this->render('_item_common', [
            'model' => $model,
            'bodyDatetime' => $model->ins_ts,
            'iconClass' => 'fa-gear bg-purple-light'
        ]);
        break;
}
