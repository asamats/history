<?php

use app\widgets\LinkPager\LinkPager;
use kartik\export\ExportMenu;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Session;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\web\View;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var Session $session
 */
?>

<?php Pjax::begin(['id' => 'grid-pjax', 'formSelector' => false]); ?>

<div class="panel panel-primary panel-small m-b-0">
    <div class="panel-body panel-body-selected">
        <div class="pull-sm-right">
            <?= Html::a(
                Yii::t('app', 'CSV'),
                Url::current(['export', 'exportType' => ExportMenu::FORMAT_CSV]),
                [
                    'class' => 'btn btn-success',
                    'data-pjax' => 0
                ]
            ) ?>
        </div>
    </div>
</div>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'pager' => [],
    'itemView' => '_item',
    'options' => [
        'tag' => 'ul',
        'class' => 'list-group'
    ],
    'itemOptions' => [
        'tag' => 'li',
        'class' => 'list-group-item'
    ],
    'emptyTextOptions' => ['class' => 'empty p-20'],
    'layout' => '{items}',
]) ?>

<?= LinkPager::widget(['dataProvider' => $dataProvider]) ?>

<?php Pjax::end(); ?>
