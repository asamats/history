<?php

use app\models\History;
use app\widgets\HistoryList\helpers\HistoryListHelper;
use yii\helpers\Html;
use app\widgets\DateTime\DateTime;

/**
 * @var History $model
 * @var string $footer
 * @var string $footerDatetime
 * @var string $bodyDatetime
 * @var string $iconClass
 */
?>
<?= Html::tag('i', '', ['class' => "icon icon-circle icon-main white $iconClass"]) ?>

<div class="bg-success ">
<?= HistoryListHelper::getBodyByModel($model) ?>

<?php if (isset($bodyDatetime)): ?>
    <span>
       <?= DateTime::widget(['dateTime' => $bodyDatetime]) ?>
    </span>
<?php endif; ?>
</div>

<?php if (isset($model->user)): ?>
    <div class="bg-info"><?= $model->user->username ?></div>
<?php endif; ?>

<?php if (isset($content) && $content): ?>
    <div class="bg-info">
        <?= $content ?>
    </div>
<?php endif; ?>

<?php if (isset($footer) || isset($footerDatetime)): ?>
    <div class="bg-warning">
        <?= $footer ?? '' ?>
        <?php if ($footerDatetime): ?>
            <span><?= DateTime::widget(['dateTime' => $footerDatetime]) ?></span>
        <?php endif; ?>
    </div>
<?php endif; ?>
