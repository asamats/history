<?php

namespace app\models\search;

use app\models\History;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * HistorySearch represents the model behind the search form about `app\models\History`.
 */
class HistorySearch extends Model
{
    public $customer_id;
    public $user_id;
    public $ins_ts;
    public $id;

    public function rules()
    {
        return [
            [[
                'customer_id',
                'user_id',
                'ins_ts',
                'id',
            ], 'safe'],
        ];

    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => Yii::t('app', 'Customer'),
            'user_id' => Yii::t('app', 'User'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = History::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'ins_ts' => SORT_DESC,
                'id' => SORT_DESC
            ],
        ]);

        $dataProvider->setPagination(false);
        $query->limit(20);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        $query->addSelect('history.*');
        $query->with([
            'customer',
            'user',
            'sms',
            'task',
            'call',
            'fax',
        ]);

        $query->andFilterWhere([
            'history.customer_id' => $this->customer_id,
            'history.user_id' => $this->user_id
        ]);

        if (isset($this->ins_ts, $this->id)) {
            $query->andWhere('(history.ins_ts, history.id) < (:ins_ts, :id)', [
                'ins_ts' => $this->ins_ts,
                'id' => $this->id
            ]);
        }

        return $dataProvider;
    }
}
