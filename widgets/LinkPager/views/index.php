<?php

use app\models\History;
use yii\helpers\Url;

/**
 * @var History $last
 */
?>
<ul class="pagination">
    <li class="next">
        <a href="<?= Url::current(['id' => $last->id, 'ins_ts' => $last->ins_ts]) ?>">
            <?= Yii::t('app', 'Next')?>
        </a>
    </li>
</ul>