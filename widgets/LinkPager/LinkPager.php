<?php

namespace app\widgets\LinkPager;

use yii\base\Widget;
use yii\data\ActiveDataProvider;

class LinkPager extends Widget
{
    /**
     * @var ActiveDataProvider $dataProvider
     */
    public $dataProvider;

    public function run()
    {
        $models = $this->dataProvider->getModels();

        $last = end($models);
        reset($models);

        echo $this->render('index', ['last' => $last]);
    }
}
