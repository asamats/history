<?php

use yii\db\Migration;

/**
 * Class m200329_160745_add_index_to_history
 */
class m200329_160745_add_index_to_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('{{%idx_history__ins_ts}}', '{{%history}}', 'ins_ts');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('{{%idx_history__ins_ts}}', '{{%history}}');
    }
}
