<?php

namespace app\controllers;

use app\models\search\HistorySearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\captcha\CaptchaAction;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
            'captcha' => [
                'class' => CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new HistorySearch();

        return $this->render('history/list', [
            'dataProvider' => $model->search(Yii::$app->request->queryParams),
            'session' => Yii::$app->session,
        ]);
    }

    /**
     * @param string $exportType
     * @return string
     */
    public function actionExport($exportType)
    {
        $model = new HistorySearch();

        // export csv immediately
        Yii::$app->request->setBodyParams([
            'exportFull_history_to_csv' => 1,
            'export_type' => $exportType,
            'column_selector_enabled' => 0,
        ]);

        return $this->render('history/export', ['dataProvider' => $model->search(Yii::$app->request->queryParams)]);
    }
}
